# Pak

### Usage
Initialize folder for pak, file in between <>:
```
cd </mnt/share-name>
pak init <git@gitlab.com:pakfs/pak.git> --output=</home/user/share-name>
```
Note: output is optional default a similar folder will be created.

Optional to make sure a NAS is updated automatically:
```
pak enable auto-update
```

Other actions
```
pak commit
pak push
pak checkout
pak pull
```
