use std::{env, fs};
use std::ffi::{c_int, OsStr};
use regex::Regex;

use serde_json;
use std::fs::File;
use std::io::prelude::*;
use std::path::{PathBuf};
use std::process::Command;
use dirs::home_dir;

use symlink::symlink_file;
use serde::{Deserialize, Serialize};
use crate::folder_structure::FolderStructure;

#[derive(Debug, Serialize, Deserialize)]
pub struct Initializer {
    origin_path: PathBuf,
    target_path: PathBuf,
    pak_path: PathBuf,
    git_path: String
}

impl Initializer {
    pub fn new(origin_path: PathBuf, git_path: &String) -> Self {
        let mut target_path = home_dir().expect("Failed to get the home directory");
        target_path = target_path.join(origin_path.file_name().unwrap());
        let pak_path = target_path.join(OsStr::new(".pak"));

        Initializer{
            origin_path: origin_path,
            target_path: target_path,
            pak_path: pak_path,
            git_path: git_path.to_string()
        }
    }

    pub fn trigger(args: Vec<String>) {
        if let Ok(current_dir) = env::current_dir() {
            if args.len() > 2 {
                let url: &String = &args[2];
                let initializer = Initializer::new(current_dir.clone(), url);
                if initializer.is_git_url() {
                    println!("Initializing Pak for {}", current_dir.display());
                    initializer.init();
                } else {
                    println!("The submitted git:pak destination does not exist...");
                }
            } else {
                println!("Please set the git:pak destination for {}", current_dir.display());
            }
        } else {
            println!("Failed to get the current working directory.");
        }
    }

    fn init(&self) {
        self.create_local();
        self.clone_public();
        self.create_config();
        self.create_status(1);
        self.push_public();
    }

    fn create_local (&self) {
        // TODO save files with hash name in .pak folder
        // TODO structure: files: hash -> content, commits: hash, changes
        env::set_current_dir(&self.origin_path)
            .expect("Failed to set working directory");
        let _ = Command::new("git")
            .arg("init")
            .output().expect("Failed to init project");
        let _ = fs::create_dir_all(self.target_path.clone());

        println!("Creating local clone in {}", self.target_path.display());
    }

    fn clone_public (&self) {
        let _ = Command::new("git")
            .arg("clone")
            .arg(self.git_path.as_str())
            .arg(self.pak_path.to_string_lossy().into_owned())
            .output().expect("Failed to link to public git repro");
        println!("Create a .pak folder in {}", self.target_path.display());
    }

    fn push_public (&self) {
        env::set_current_dir(&self.pak_path)
            .expect("Failed to set working directory");
        let _ = Command::new("git")
            .arg("add")
            .arg(".pakstatus.json")
            .arg(".pakconfig.json")
            .output().expect("Failed to add files to git");
        let _ = Command::new("git")
            .arg("commit")
            .arg("-m")
            .arg("\"Updated pak files\"")
            .output().expect("Failed to commit");
        let _ = Command::new("git")
            .arg("push")
            .arg("origin")
            .arg("main")
            .output().expect("Failed to push");
        println!("Pushed pak status");
    }

    fn create_status (&self, depth: c_int) {
        let data = FolderStructure::from_path(&self.origin_path);

        // TODO move to folderstructure
        for file in data.clone().files {
            let origin_path = self.origin_path.join(OsStr::new(&file));
            let target_path = self.target_path.join(OsStr::new(&file));
            let _ = symlink_file(origin_path, target_path);
        }
        for folder in data.clone().folders {
            let target_path = self.target_path.join(OsStr::new(&folder.name));
            println!("New folder {}", target_path.display());
            let _ = fs::create_dir_all(target_path);
        }


        let json_str = serde_json::to_string(&data).expect("Failed to serialize data into JSON");

        let pak_status_path = self.pak_path.join(OsStr::new(".pakstatus.json"));
        let mut file = File::create(pak_status_path).expect("Failed to create file");
        file.write_all(json_str.as_bytes()).expect("Failed to write to file");

        println!("Saved current project status using depth {}", depth);
    }

    fn create_config (&self) {
        let json_str = serde_json::to_string(&self).expect("Failed to serialize data into JSON");

        let pak_config_path = self.pak_path.join(OsStr::new(".pakconfig.json"));
        let mut file = File::create(pak_config_path.clone()).expect("Failed to create file");
        file.write_all(json_str.as_bytes()).expect("Failed to write to file");

        println!("Saved project config in {}", pak_config_path.display());
    }

    fn is_git_url(&self) -> bool {
        let git_ssh_url_regex = Regex::new(r#"^(git@)?[^\s:]+:[^\s/]+/.+\.git$"#).unwrap();
        git_ssh_url_regex.is_match(&self.git_path)
    }
}