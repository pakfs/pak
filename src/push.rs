use std::env;
use std::ffi::c_int;
use regex::Regex;

use serde_json;
use std::fs::File;
use std::io::prelude::*;
use std::path::PathBuf;
use serde::{Deserialize, Serialize};
use crate::folder_structure::FolderStructure;

#[derive(Debug, Serialize, Deserialize)]
pub struct Initializer {
    origin_path: PathBuf,
    git_path: String
}

impl Initializer {
    pub fn new(current_dir: PathBuf, git_path: &String) -> Self {
        Initializer{
            origin_path: current_dir,
            git_path: git_path.to_string()
        }
    }

    pub fn trigger(args: Vec<String>) {
        if let Ok(current_dir) = env::current_dir() {
            if args.len() > 2 {
                let url: &String = &args[2];
                let initializer = Initializer::new(current_dir.clone(), url);
                if initializer.is_git_url() {
                    println!("Initializing Pak for {}", current_dir.display());
                    initializer.init();
                } else {
                    println!("The submitted git:pak destination does not exist...");
                }
            } else {
                println!("Please set the git:pak destination for {}", current_dir.display());
            }
        } else {
            println!("Failed to get the current working directory.");
        }
    }

    fn init(&self) {
        self.create_local();
        self.clone_public();
        self.create_config();
        self.create_status(1);
        self.push_public();
    }

    fn create_local (&self) {
        println!("Create a reference copy for every single file in /home/user/current_folder")
    }

    fn clone_public (&self) {
        println!("Create a .pak folder in the local folder")
    }

    fn push_public (&self) {
        println!("Push the changes in the .pak folder to the public git")
    }

    fn create_status (&self, depth: c_int) {
        let data = FolderStructure::from_path(&self.origin_path);

        let json_str = serde_json::to_string(&data).expect("Failed to serialize data into JSON");

        // Write the JSON string to a file
        let mut file = File::create(".pakstatus.json").expect("Failed to create file");
        file.write_all(json_str.as_bytes()).expect("Failed to write to file");
        println!("Saved current project status...");
    }

    fn create_config (&self) {
        let json_str = serde_json::to_string(&self).expect("Failed to serialize data into JSON");

        // Write the JSON string to a file
        let mut file = File::create(".pakconfig.json").expect("Failed to create file");
        file.write_all(json_str.as_bytes()).expect("Failed to write to file");
        println!("Saved project config...");
    }

    fn is_git_url(&self) -> bool {
        let git_ssh_url_regex = Regex::new(r#"^(git@)?[^\s:]+:[^\s/]+/.+\.git$"#).unwrap();
        git_ssh_url_regex.is_match(&self.git_path)
    }
}