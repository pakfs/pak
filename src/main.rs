mod init;
mod folder_structure;

use std::env;
use init::Initializer;

fn help() {
    println!("Some help menu here...");
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() > 1 {
        let action: &String = &args[1];
        match action.as_str() {
            "init" => Initializer::trigger(args),
            _ => help(),
        }
    } else {
        help();
    }
}
