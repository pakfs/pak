use serde::{Serialize, Deserialize};
use std::fs;
use std::path::{PathBuf};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct FolderStructure {
    pub name: String,
    pub files: Vec<String>,
    pub folders: Vec<FolderStructure>,
}
impl FolderStructure {
    pub fn from_path(path: &PathBuf) -> FolderStructure {
        let mut files = Vec::new();
        let mut folders = Vec::new();

        if let Ok(entries) = fs::read_dir(path) {
            for entry in entries {
                if let Ok(entry) = entry {
                    let entry_path = entry.path();
                    if entry_path.is_file() {
                        if let Some(file_name) = entry_path.file_name() {
                            if let Some(file_name) = file_name.to_str() {
                                files.push(file_name.to_string());
                            }
                        }
                    } else if entry_path.is_dir() {
                        folders.push(FolderStructure::from_path(&entry_path));
                    }
                }
            }
        }

        let name = path.file_name().unwrap().to_str().unwrap().to_string();
        FolderStructure { name, files, folders }
    }

    pub fn link(target: &PathBuf) {

    }
}
